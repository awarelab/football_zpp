from mrunner.helpers.specification_helper import create_experiments_helper

from munch import Munch

base_config = {
    'use_gae': True,
    'lambda': 0.95,
    'kl_coeff': 0.0,
    'clip_rewards': False,
    'vf_clip_param': 10.0,
    'entropy_coeff': 0.003,
    "clip_param": 0.2,

    'num_workers': 16,
    'num_envs_per_worker': 1,
    'train_batch_size': 512*16,
    'sgd_minibatch_size': 512*16/8,
    'num_sgd_iter': 8,

    'batch_mode': 'truncate_episodes',
    'observation_filter': 'NoFilter',
    'grad_clip': 0.64,
    'vf_loss_coeff': 0.5,
    'num_gpus': 0,
    'lr': 0.00016,
    'lr_schedule': None,
    'shuffle_sequences': True,
    'training_iteration': 5000,
    'model': {"custom_model": "my_model_pm", "custom_options": {},},
    'env_config': Munch(env_name='fill_me', representation='simple115', stacked=False,
                               rewards='scoring,checkpoints', # logdir=logger.get_dir(),
                               write_goal_dumps=False, write_full_episode_dumps=False, render=False, dump_frequency=0),
    'env_config.env_name': '1_vs_1_easy',
    'env': "MyConfigurableFootball"

}

params_grid = {'env_config.env_name': ["academy_empty_goal_close",
                                         "academy_empty_goal",
                                         "academy_run_to_score",
                                         "academy_run_to_score_with_keeper",
                                         "academy_pass_and_shoot_with_keeper",
                                         "academy_run_pass_and_shoot_with_keeper",
                                         "academy_3_vs_1_with_keeper",
                                         "academy_corner",
                                         "academy_counterattack_easy",
                                         "academy_counterattack_hard",
                                         "academy_single_goal_versus_lazy"]}

experiments_list = create_experiments_helper(experiment_name='rllib_first blood',
                                             base_config=base_config, params_grid=params_grid,
                                             script='python3 gfootball/examples/run_rllib.py',
                                             env={},
                                             exclude=['third_party', ".idea", ],
                                             python_path='.',
                                             tags=[globals()['script'][:-3]],
                                             with_neptune=True)