from mrunner.helpers.specification_helper import create_experiments_helper

from munch import Munch

base_config = {
    'use_gae': True,
    'lambda': 0.95,
    'kl_coeff': 0.0,
    'clip_rewards': False,
    'vf_clip_param': 10.0,
    'entropy_coeff': 0.003,
    "clip_param": 0.2,

    'num_workers': 16,
    'num_envs_per_worker': 1,
    'train_batch_size': 512*16,
    'sgd_minibatch_size': 512*16/8,
    'num_sgd_iter': 8,

    'batch_mode': 'truncate_episodes',
    'observation_filter': 'NoFilter',
    'grad_clip': 0.64,
    'vf_loss_coeff': 0.5,
    'num_gpus': 0,
    'lr': 0.00016,
    'lr_schedule': None,
    'shuffle_sequences': True,
    'training_iteration': 100000,
    'model': {"custom_model": "my_model_pm", "custom_options":{},},
    'env_config': Munch(env_name='fill_me', stacked=False,
                               rewards='scoring,checkpoints',
                               write_goal_dumps=False, write_full_episode_dumps=False, render=False, dump_frequency=0),
    'model.custom_options.layers_sizes': [128, 128],
    'env_config.extractors': ["simple_115_fn"],
    'env': "MyConfigurableFootballWithObservation",
}

params_grid = {'env_config.env_name': ['5_vs_5_stochastic', '5_vs_5_easy_stochastic', '5_vs_5_hard_stochastic'],
               'model.custom_options.layers_sizes': [[128, 128]],
               'checkpoint_value_decrease_threshold': [50.0]
}

experiments_list = create_experiments_helper(experiment_name='imitation 1.0',
                                             base_config=base_config, params_grid=params_grid,
                                             script='python3 gfootball/examples/run_rllib.py',
                                             env={},
                                             exclude=['third_party', ".idea", ],
                                             python_path='.',
                                             tags=[globals()['script'][:-3]],
                                             with_neptune=True)