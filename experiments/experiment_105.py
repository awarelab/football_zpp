from mrunner.helpers.specification_helper import create_experiments_helper

#  __11_vs_11_stochastic__ A full 90 minutes football game (medium difficulty)
#    * __11_vs_11_easy_stochastic__ A full 90 minutes football game (easy difficulty)
#    * __11_vs_11_hard_stochastic__

base_config = {
    'level': '5_vs_5_easy_stochastic',
    'reward_experiment': 'scoring,checkpoints',
    'representation': 'simple115',
    'stacked': False,
    'policy': 'mlp',
    'num_timesteps': int(50000000),
    'num_envs': 16,
    'nsteps': 512,
    'noptepochs': 2,
    'nminibatches': 8,
    'save_interval': 1000,
    'seed': 0,
    'lr': 0.000343,
    'ent_coef': 0.003,
    'gamma': 0.993,
    'cliprange': 0.08,
    'max_grad_norm': 0.64,
    'render': False,
    'dump_full_episodes': True,
    'dump_scores': False,
    'load_path': None,
}

params_grid = {'level': ['1_vs_1_easy',
                         '5_vs_5',
                         '11_vs_11_stochastic',
                         '11_vs_11_easy_stochastic',
                         '11_vs_11_hard_stochastic'], 'noptepochs': [2, 4, 8, 16]}

experiments_list = create_experiments_helper(experiment_name='simple115 tuning',
                                             base_config=base_config, params_grid=params_grid,
                                             script='python3 gfootball/examples/run_ppo2.py',
                                             env={"OPENAI_LOG_FORMAT": "log"},
                                             exclude=[],
                                             python_path='.',
                                             tags=[globals()['script'][:-3]],
                                             with_neptune=True)