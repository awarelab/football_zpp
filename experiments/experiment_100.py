from mrunner.helpers.specification_helper import create_experiments_helper

# "11_vs_11_stochastic", "11_vs_11_easy_stochastic", "11_vs_11_hard_stochastic",
# "academy_empty_goal_close", "academy_empty_goal", "academy_run_to_score", "academy_run_to_score_with_keeper", "academy_pass_and_shoot_with_keeper", "academy_run_pass_and_shoot_with_keeper", "academy_3_vs_1_with_keeper", "academy_corner", "academy_counterattack_easy", "academy_counterattack_hard", "academy_single_goal_versus_lazy"

base_config = {
    'level': 'academy_empty_goal_close',
    'representation': 'extracted',
    'stacked': False,
    'state': 'extracted_stacked',
    'reward_experiment': 'scoring',
    'policy': 'impala_cnn',
    'num_timesteps': int(1e7),
    'num_envs': 16,
    'nsteps': 128,
    'noptepochs': 4,
    'nminibatches': 8,
    'save_interval': 100,
    'seed': 0,
    'lr': 0.00008,
    'ent_coef': 0.01,
    'gamma': 0.993,
    'cliprange': 0.27,
    'max_grad_norm': 0.5,
    'render': False,
    'dump_full_episodes': False,
    'dump_scores': False,
    'load_path': None,
}

params_grid = {'level': ["academy_empty_goal_close",
                         "academy_empty_goal",
                         "academy_run_to_score",
                         "academy_run_to_score_with_keeper",
                         "academy_pass_and_shoot_with_keeper",
                         "academy_run_pass_and_shoot_with_keeper",
                         "academy_3_vs_1_with_keeper",
                         "academy_corner",
                         "academy_counterattack_easy",
                         "academy_counterattack_hard",
                         "academy_single_goal_versus_lazy"],
               'reward_experiment': ['scoring', 'scoring,checkpoints']}

experiments_list = create_experiments_helper(experiment_name='MonteCarloAgent in gfootball',
                                             base_config=base_config, params_grid=params_grid,
                                             script='python3 gfootball/examples/run_ppo2.py',
                                             exclude=[],
                                             python_path='',
                                             tags=[globals()['script'][:-3]],
                                             with_neptune=True)