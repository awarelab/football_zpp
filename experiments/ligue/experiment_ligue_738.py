from mrunner.helpers.specification_helper import create_experiments_helper

from munch import Munch

base_config = {
    "agent_dir": "/football/08_02-21_48-peaceful_brahmagupta/rllib-first-blood_fbi7",
    "username": "loss",
    "token": "RXmrNOaXAS_1581330182",
    "model_name": "pan_clex_easy",
    "track": "11vs11",
    "how_many": 10,
    "delay": 10

}

params_grid = {}

experiments_list = create_experiments_helper(experiment_name='ligue',
                                             base_config=base_config, params_grid=params_grid,
                                             script='python3 gfootball/examples/play_remote_deployable.py',
                                             env={},
                                             exclude=['third_party', ".idea", ],
                                             python_path='.',
                                             tags=[globals()['script'][:-3]],
                                             with_neptune=True)