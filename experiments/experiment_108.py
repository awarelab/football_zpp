from mrunner.helpers.specification_helper import create_experiments_helper

#  __11_vs_11_stochastic__ A full 90 minutes football game (medium difficulty)
#    * __11_vs_11_easy_stochastic__ A full 90 minutes football game (easy difficulty)
#    * __11_vs_11_hard_stochastic__
from entry_points import get_PMStateWrapper

base_config = {
    'level': '5_vs_5_easy_stochastic',
    'reward_experiment': 'scoring,checkpoints',
    'representation': 'raw',
    'custom_wrapper': get_PMStateWrapper,
    'get_PMStateWrapper.extractors': ["left_team_fn", "left_team_direction_fn",
                                      "right_team_fn", "right_team_direction_fn",
                                      "ball_fn", "ball_direction_fn", "ball_owned_team_one_hot_fn",
                                      "active_one_hot_fn", "game_mode_one_hot_fn"],
    'NeptuneLogger.smooth_channels': {"eprewmean": (0.99, 0.0)},
    'stacked': False,
    'policy': 'gfootball_interaction_networks',
    'num_timesteps': int(5000000),
    'num_envs': 16,
    'nsteps': 512,
    'noptepochs': 32,
    'nminibatches': 8,
    'save_interval': 1,
    'seed': 0,
    'lr': 0.00016,
    'ent_coef': 0.003,
    'gamma': 0.993,
    'cliprange': 0.08,
    'max_grad_norm': 0.64,
    'render': False,
    'dump_full_episodes': True,
    'dump_scores': False,
    'load_path': None,

}

params_grid = {'level': ["academy_run_to_score_with_keeper",
                         "academy_pass_and_shoot_with_keeper",
                         "academy_run_pass_and_shoot_with_keeper",
                         "academy_3_vs_1_with_keeper",
                         "academy_counterattack_easy",],
               'reward_experiment': ['scoring,checkpoints'],
               'interactive_mlp.p_num_hidden': [4, 8]}

experiments_list = create_experiments_helper(experiment_name='pm_extractor debug run',
                                             base_config=base_config, params_grid=params_grid,
                                             script='python3 gfootball/examples/run_ppo2.py',
                                             env={"OPENAI_LOG_FORMAT": "log"},
                                             exclude=[],
                                             python_path='.',
                                             tags=[globals()['script'][:-3]],
                                             with_neptune=True)