from __future__ import absolute_import, division, print_function, unicode_literals

import tensorflow as tf

from tensorflow import keras
import numpy as np

from ray.rllib.agents.ppo import PPOAgent
from munch import Munch
import ray
import random

from absl import app
from absl import flags
from absl import logging
import gfootball.env as football_env
from gfootball.env import football_action_set
import grpc
import numpy as np
# import tensorflow.compat.v2 as tf
from gfootball.examples.run_rllib import MyConfigurableFootball

if __name__ == '__main__':
  config =  {'model': {"custom_model": "my_model_pm", 'custom_options': {'layers_sizes': [128, 128]},},
             'env_config': Munch(env_name='1_vs_1_easy', representation='simple115', stacked=False,
                                 rewards='scoring,checkpoints', # logdir=logger.get_dir(),
                                 write_goal_dumps=False, write_full_episode_dumps=False, render=False,
                                 dump_frequency=0),}


  ray.init(num_gpus=0)
  config["env"] = MyConfigurableFootball
  alg = PPOAgent(config=config)
  alg.restore("checkpoints/easy_128_128/checkpoint-27000")
  default_policy_1 = alg.get_policy('default_policy')

  res = alg.compute_action(np.zeros(115), full_fetch=True)
  action = int(res[0])

  username = "loss"
  token = "RXmrNOaXAS_1581330182"
  model_name = "uschatec"
  track = "11vs11"
  how_many = 10

  env = football_env.create_remote_environment(
      username, token, model_name, track=track,
      representation='simple115', stacked=False,
      include_rendering=False)


  for _ in range(how_many):
    ob = env.reset()
    cnt = 1
    done = False
    while not done:
      try:
        res = alg.compute_action(ob, full_fetch=True)
        action = action = int(res[0])
        ob, rew, done, _ = env.step(action)
        print('Playing the game, step {}, action {}, rew {}, done {}'.format(
                     cnt, action, rew, done))
        cnt += 1
      except grpc.RpcError as e:
        print(e)
        break
    print('=' * 50)


  alg_2 = PPOAgent(config=config)
  alg_2.restore("checkpoints/easy_128_128/checkpoint-27000")
  default_policy_2 = alg_2.get_policy('default_policy')
  #
  # print(alg.compute_action(np.zeros(115), full_fetch=True))
  # print(alg_2.compute_action(np.zeros(115), full_fetch=True))
  #

  # default_policy.export_checkpoint("checkpoints/stochastic_128_128")
