# coding=utf-8
# Copyright 2019 Google LLC
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Runs football_env on OpenAI's ppo2."""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import multiprocessing
import os

from baselines import logger
from baselines.bench import monitor
from baselines.common.vec_env.subproc_vec_env import SubprocVecEnv
from baselines.ppo2 import ppo2
import gfootball.env as football_env
import tensorflow as tf
from mrunner.helpers.client_helper import logger as neptune_logger
import gin
from mrunner.helpers.client_helper import get_configuration

from gfootball.examples.models import * #This is to make the registration to work


@gin.configurable
class NeptuneLogger(logger.KVWriter):

    def __init__(self, smooth_channels={}):
        self.smooth_channels = smooth_channels

    def writekvs(self, kvs):
        for k, v in sorted(kvs.items()):
            if hasattr(v, 'dtype'):
                v = float(v)
            neptune_logger(k, v)
            if k in self.smooth_channels:
                smooth_param, accumulator = self.smooth_channels[k]
                accumulator = smooth_param*accumulator + (1-smooth_param)*v
                self.smooth_channels[k] = (smooth_param, accumulator)
                neptune_logger(rf"{k}_smooth_{smooth_param}", accumulator)


FLAGS = None

def create_single_football_env(iprocess):
  """Creates gfootball environment."""
  global FLAGS
  env = football_env.create_environment(
      env_name=FLAGS.level,
      representation=FLAGS.representation,
      stacked=FLAGS.stacked,
      rewards=FLAGS.reward_experiment,
      logdir=logger.get_dir(),
      write_goal_dumps=FLAGS.dump_scores and (iprocess == 0),
      write_full_episode_dumps=FLAGS.dump_full_episodes and (iprocess == 0),
      render=FLAGS.render and (iprocess == 0),
      dump_frequency=50 if FLAGS.render and iprocess == 0 else 0)
  if "custom_wrapper" in FLAGS:
    env = FLAGS.custom_wrapper(env)
  env = monitor.Monitor(env, logger.get_dir() and os.path.join(logger.get_dir(),
                                                               str(iprocess)))
  return env


checkpoint_saver = None

def save_tf_checkpoint(update_no):
    global FLAGS, checkpoint_saver

    if checkpoint_saver is None:
        checkpoint_saver = tf.train.Saver(var_list=tf.global_variables(), max_to_keep=None)

    if update_no > 1 and update_no%FLAGS.save_interval == 0:
        sess = tf.get_default_session()
        periodic_savepath = os.path.join(".", '%.5i' % update_no)

        checkpoint_saver.save(sess, periodic_savepath, write_state=False)
        checkpoint_saver.restore()


def load_tf_checkpoint(update_no):
    global FLAGS, checkpoint_saver

    if checkpoint_saver is None:
        checkpoint_saver = tf.train.Saver(var_list=tf.global_variables(), max_to_keep=None)

    if update_no == 1:
        import glob
        path = "/tmp"
        search_path = path + "*index"
        files = glob.glob(search_path)
        to_restore = sorted(files)[-1]
        to_restore = to_restore[:-6]
        print("Path to restore", to_restore)

        sess = tf.get_default_session()
        checkpoint_saver.restore(sess, to_restore)


def train():
  global FLAGS

  FLAGS = get_configuration(with_neptune=True, inject_parameters_to_gin=True)
  create_single_football_env(1) #Hack, to make all of wrappers to be created. Needed for PMStateWrapper.

  logger_ = logger.get_current()
  logger_.output_formats.append(NeptuneLogger())

  ncpu = multiprocessing.cpu_count()
  config = tf.ConfigProto(allow_soft_placement=True,
                          intra_op_parallelism_threads=ncpu,
                          inter_op_parallelism_threads=ncpu)
  config.gpu_options.allow_growth = True
  tf.Session(config=config).__enter__()

  vec_env = SubprocVecEnv([
      (lambda _i=i: create_single_football_env(_i))
      for i in range(FLAGS.num_envs)
  ], context=None)

  ppo2.learn(network=FLAGS.policy,
             total_timesteps=FLAGS.num_timesteps,
             env=vec_env,
             seed=FLAGS.seed,
             nsteps=FLAGS.nsteps,
             nminibatches=FLAGS.nminibatches,
             noptepochs=FLAGS.noptepochs,
             max_grad_norm=FLAGS.max_grad_norm,
             gamma=FLAGS.gamma,
             ent_coef=FLAGS.ent_coef,
             lr=FLAGS.lr,
             log_interval=1,
             save_interval=FLAGS.save_interval,
             cliprange=FLAGS.cliprange,
             load_path=FLAGS.load_path,
             update_fn=save_tf_checkpoint)

if __name__ == '__main__':
  train()
