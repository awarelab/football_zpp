# coding=utf-8
# Copyright 2019 Google LLC
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Additional models, not available in OpenAI baselines.

gfootball_impala_cnn is architecture used in the paper
(https://arxiv.org/pdf/1907.11180.pdf).
It is illustrated in the appendix. It is similar to Large architecture
from IMPALA paper; we use 4 big blocks instead of 3 though.
"""
import gin
from baselines.common.models import register
import sonnet as snt
import tensorflow as tf
from baselines.a2c.utils import conv, fc, conv_to_fc, batch_to_seq, seq_to_batch
import numpy as np

@register('gfootball_impala_cnn')
def gfootball_impala_cnn():
  def network_fn(frame):
    # Convert to floats.
    frame = tf.to_float(frame)
    frame /= 255
    with tf.variable_scope('convnet'):
      conv_out = frame
      conv_layers = [(16, 2), (32, 2), (32, 2), (32, 2)]
      for i, (num_ch, num_blocks) in enumerate(conv_layers):
        # Downscale.
        conv_out = snt.Conv2D(num_ch, 3, stride=1, padding='SAME')(conv_out)
        conv_out = tf.nn.pool(
            conv_out,
            window_shape=[3, 3],
            pooling_type='MAX',
            padding='SAME',
            strides=[2, 2])

        # Residual block(s).
        for j in range(num_blocks):
          with tf.variable_scope('residual_%d_%d' % (i, j)):
            block_input = conv_out
            conv_out = tf.nn.relu(conv_out)
            conv_out = snt.Conv2D(num_ch, 3, stride=1, padding='SAME')(conv_out)
            conv_out = tf.nn.relu(conv_out)
            conv_out = snt.Conv2D(num_ch, 3, stride=1, padding='SAME')(conv_out)
            conv_out += block_input

    conv_out = tf.nn.relu(conv_out)
    conv_out = snt.BatchFlatten()(conv_out)

    conv_out = snt.Linear(256)(conv_out)
    conv_out = tf.nn.relu(conv_out)

    return conv_out

  return network_fn


@register('gfootball_interaction_networks')
@gin.configurable
def interactive_mlp(num_layers=2, num_hidden=64, p_num_layers=1, p_num_hidden=16, activation=tf.tanh, layer_norm=False):
  from gfootball.examples.wrappers import PMStateWrapper

  def network_fn(X):
    h = tf.layers.flatten(X)
    split = PMStateWrapper.split_tensor(h)

    left_players_num = int(split['left_team'].shape[1].value/2)
    left_team_players_pos = tf.split(split['left_team'], [2]*left_players_num, axis=1)
    left_team_players_direction = tf.split(split['left_team_direction'], [2]*left_players_num, axis=1)

    right_players_num = int(split['right_team'].shape[1].value/2)
    right_team_players_pos = tf.split(split['right_team'], [2]*right_players_num, axis=1)
    right_team_players_direction = tf.split(split['right_team_direction'], [2]*right_players_num, axis=1)

    left_team_players = [tf.concat(p, axis=1) for p in zip(left_team_players_pos, left_team_players_direction)]
    right_team_players = [tf.concat(p, axis=1) for p in zip(right_team_players_pos, right_team_players_direction)]

    intraction_features = []
    for player_1 in left_team_players:
      for player_2 in right_team_players:
        input = tf.concat((player_1, player_2), axis=1)
        for i in range(p_num_layers):
          p_h = fc(input, 'interactive_mlp_fc_reuse_{}'.format(i), nh=p_num_hidden, init_scale=np.sqrt(2))
          if layer_norm:
            p_h = tf.contrib.layers.layer_norm(p_h, center=True, scale=True)
          p_h = activation(p_h)

        intraction_features.append(p_h)


    del split['left_team']
    del split['left_team_direction']
    del split['right_team']
    del split['right_team_direction']

    all_features = intraction_features + list(split.values())
    h = tf.concat(all_features, axis=1)
    for i in range(num_layers):
      h = fc(h, 'interactive_mlp_fc{}'.format(i), nh=num_hidden, init_scale=np.sqrt(2))
      if layer_norm:
        h = tf.contrib.layers.layer_norm(h, center=True, scale=True)
      h = activation(h)

    return h

  return network_fn

@register('gfootball_embedding_networks')
@gin.configurable
def embedding_mlp(num_layers=2, num_hidden=64, p_num_layers=1, p_num_hidden=16, activation=tf.tanh, layer_norm=False):
  from gfootball.examples.wrappers import PMStateWrapper

  def network_fn(X):
    h = tf.layers.flatten(X)
    split = PMStateWrapper.split_tensor(h)

    left_players_num = int(split['left_team'].shape[1].value/2)
    left_team_players_pos = tf.split(split['left_team'], [2]*left_players_num, axis=1)
    left_team_players_direction = tf.split(split['left_team_direction'], [2]*left_players_num, axis=1)

    right_players_num = int(split['right_team'].shape[1].value/2)
    right_team_players_pos = tf.split(split['right_team'], [2]*right_players_num, axis=1)
    right_team_players_direction = tf.split(split['right_team_direction'], [2]*right_players_num, axis=1)

    left_team_players = [tf.concat(p, axis=1) for p in zip(left_team_players_pos, left_team_players_direction)]
    right_team_players = [tf.concat(p, axis=1) for p in zip(right_team_players_pos, right_team_players_direction)]

    player_features = []
    p_h = None
    for player in left_team_players:
      input = player
      for i in range(p_num_layers):
        p_h = fc(input, 'embedding_mlp_fc_left_reuse_{}'.format(i), nh=p_num_hidden, init_scale=np.sqrt(2))
        if layer_norm:
          p_h = tf.contrib.layers.layer_norm(player, center=True, scale=True)
      p_h = activation(p_h)

      player_features.append(p_h)

    for player in right_team_players:
      input = player
      for i in range(p_num_layers):
        p_h = fc(input, 'embedding_mlp_fc_right_reuse_{}'.format(i), nh=p_num_hidden, init_scale=np.sqrt(2))
        if layer_norm:
          p_h = tf.contrib.layers.layer_norm(player, center=True, scale=True)
      p_h = activation(p_h)

      player_features.append(p_h)


    del split['left_team']
    del split['left_team_direction']
    del split['right_team']
    del split['right_team_direction']

    all_features = player_features + list(split.values())
    h = tf.concat(all_features, axis=1)
    for i in range(num_layers):
      h = fc(h, 'interactive_mlp_fc{}'.format(i), nh=num_hidden, init_scale=np.sqrt(2))
      if layer_norm:
        h = tf.contrib.layers.layer_norm(h, center=True, scale=True)
      h = activation(h)

    return h

  return network_fn