from __future__ import absolute_import, division, print_function, unicode_literals

import tensorflow as tf

from tensorflow import keras
import numpy as np

from ray.rllib.agents.ppo import PPOAgent
from munch import Munch
import ray

from gfootball.examples.run_rllib import MyConfigurableFootball

if __name__ == '__main__':
  config =  {'model': {"custom_model": "my_model_pm", 'custom_options': {'layers_sizes': [128, 128]},},
             'env_config': Munch(env_name='1_vs_1_easy', representation='simple115', stacked=False,
                                 rewards='scoring,checkpoints', # logdir=logger.get_dir(),
                                 write_goal_dumps=False, write_full_episode_dumps=False, render=False,
                                 dump_frequency=0),}


  ray.init(num_gpus=0)
  config["env"] = MyConfigurableFootball
  alg = PPOAgent(config=config)
  alg.restore("checkpoints/stochastic_128_128/checkpoint-33000")
  default_policy_1 = alg.get_policy('default_policy')


  alg_2 = PPOAgent(config=config)
  alg_2.restore("checkpoints/easy_128_128/checkpoint-27000")
  default_policy_2 = alg_2.get_policy('default_policy')
  #
  # print(alg.compute_action(np.zeros(115), full_fetch=True))
  # print(alg_2.compute_action(np.zeros(115), full_fetch=True))
  #

  # default_policy.export_checkpoint("checkpoints/stochastic_128_128")
