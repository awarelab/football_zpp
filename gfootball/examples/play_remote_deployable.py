from __future__ import absolute_import, division, print_function, unicode_literals

from mrunner.helpers.client_helper import get_configuration
from munch import Munch
import ray
from ray.rllib.agents.ppo import PPOAgent
import grpc
import pathlib
import sys
import cloudpickle
from random import randint
import time


if __name__ == '__main__':
    config = get_configuration(with_neptune=True, inject_parameters_to_gin=False)
    agent_dir = config.pop("agent_dir")
    username = config.pop("username")
    token = config.pop("token")
    model_name = config.pop("model_name")
    track = config.pop("track")
    how_many = config.pop("how_many")
    delay = config.pop("delay")

    sys.path.insert(0, agent_dir)
    print(sys.path)
    from gfootball.examples.run_rllib import MyConfigurableFootballWithObservation, overwrite_with_dots
    import gfootball.env as football_env

    config_file_gen = pathlib.Path(agent_dir).glob('**/config.pkl')
    # config_file_genaa = pathlib.Path(agent_dir).glob('*')
    print("agent_dir", agent_dir)
    # print("pathlib.Path(agent_dir)", pathlib.Path(agent_dir))
    # print("pathlib.Path(agent_dir).interdir", list(pathlib.Path(agent_dir).iterdir()))
    # print("agent_diraa", list(config_file_genaa))
    # print("config_file_gen", list(config_file_gen))
    config_file = next(config_file_gen)
    checkpoints_tmp = (pathlib.Path(agent_dir) / "PPO").glob('**/checkpoint*.tune_metadata')
    checkpoints_dirs = [path.parent for path in checkpoints_tmp]
    checkpoints_root = checkpoints_dirs[0].parent
    checkpoints_names = [path.parts[-1][11:] for path in checkpoints_dirs]
    last_checkpoint_num = sorted(checkpoints_names, key=int, reverse=True)[0]

    checkpoint_to_restore = str(
        checkpoints_root / ("checkpoint_" + last_checkpoint_num) / ("checkpoint-" + last_checkpoint_num))

    with open(config_file, "rb") as f:
        config = Munch(cloudpickle.load(f))
        config = config.parameters

    overwrite_with_dots(config)

    ray.init(num_gpus=0)
    config["env"] = MyConfigurableFootballWithObservation
    config['num_workers'] = 0
    config['train_batch_size'] = 2000
    config['sgd_minibatch_size'] = 500

    alg = None
    while alg is None:
        try:
            alg = PPOAgent(config=config)
        except Exception as e:
            if "Unknown config" in e.args[0]:
                param = e.args[0].split("`")[1]
                config.pop(param)
                print("Removing:", param)
            else:
                raise e
    alg.restore(checkpoint_to_restore)

    env = football_env.create_remote_environment(
        username, token, model_name, track=track,
        representation='simple115', stacked=False,
        include_rendering=False)

    for _ in range(how_many):
        ob = env.reset()
        cnt = 1
        done = False
        while not done:
            try:
                res = alg.compute_action(ob, full_fetch=True)
                action = action = int(res[0])
                ob, rew, done, _ = env.step(action)
                if rew != 0:
                    print("Goal")
                print('Playing the game, step {}, action {}, rew {}, done {}'.format(
                    cnt, action, rew, done))
                cnt += 1
            except grpc.RpcError as e:
                print(e)
                break
        print('=' * 50)
        time.sleep(randint(0, delay))