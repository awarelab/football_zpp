import gym
import numpy as np
from munch import Munch

def one_hot(i, num_classes):
    vec = [0]*num_classes
    if i is not None:
        vec[i] = 1

    return np.asarray(vec)


def team_build_v1(pos, dir_, team_id, ball_pos, ball_dir, ball_owned_team, active_one=None):
    pos = np.reshape(pos, (-1, 2))
    dir_ = np.reshape(dir_, (-1, 2))
    team_id_np = np.ones(shape=(11, 1)) * team_id
    active_one_hot = np.zeros(shape=(11, 1))
    if active_one is not None:
        active_one_hot[active_one] = 1

    ball_owned_team_np = np.broadcast_to(one_hot(ball_owned_team, 3), shape=(11, 3))
    ball_pos_np = np.broadcast_to(ball_pos[:2], shape=(11, 2))
    ball_dir_np = np.broadcast_to(ball_dir[:2], shape=(11, 2))
    res = np.concatenate([pos, dir_, active_one_hot, team_id_np, ball_pos_np, ball_owned_team_np, ball_dir_np], axis=1)
    return np.reshape(res, (-1,))


class PMStateWrapper(gym.Wrapper):
  """A wrapper that converts an observation to 115-features state."""

  extractors_fns = Munch(
      left_team_full_v1_fn=lambda obs: team_build_v1(obs['left_team'], obs['left_team_direction'], 0, obs['ball'], obs['ball_direction'], obs['ball_owned_team']+1, obs['active']),
      right_team_full_v1_fn=lambda obs: team_build_v1(obs['right_team'], obs['right_team_direction'], 1, obs['ball'], obs['ball_direction'], obs['ball_owned_team']+1, None),
      left_team_fn=lambda obs: obs['left_team'].flatten(),
      left_team_direction_fn=lambda obs: obs['left_team_direction'].flatten(),
      right_team_fn=lambda obs: obs['right_team'].flatten(),
      right_team_direction_fn=lambda obs: obs['right_team_direction'].flatten(),
      ball_fn=lambda obs: obs['ball'],
      ball_reduced_fn=lambda obs: obs['ball'][:2],
      ball_direction_fn=lambda obs: obs['ball_direction'],
      ball_direction_reduced_fn=lambda obs: obs['ball_direction'][:2],
      ball_owned_team_one_hot_fn=lambda obs: one_hot(obs['ball_owned_team']+1, 3),
      active_one_hot_fn=lambda obs: one_hot(obs['active'], 11),
      game_mode_one_hot_fn=lambda obs: one_hot(obs['game_mode'], 7),
      simple_115_fn=lambda obs: np.concatenate([obs['left_team'].flatten(), obs['left_team_direction'].flatten(),
                                                obs['right_team'].flatten(), obs['right_team_direction'].flatten(),
                                                obs['ball'], obs['ball_direction'],
                                                one_hot(obs['ball_owned_team']+1, 3), one_hot(obs['active'], 11),
                                                one_hot(obs['game_mode'], 7)])
  )

  # This is a hack but for a good cause ;)
  features_dims = None
  features_extractors = None

  @staticmethod
  def split_tensor(tensor):
      import tensorflow as tf
      assert PMStateWrapper.features_dims is not None, "You need to instantiate first. This is a hack. We are sorry"
      assert len(PMStateWrapper.features_dims)==1, "Only one player supported. Todo ;p"
      split_sizes = PMStateWrapper.features_dims[0]
      x = tf.split(tensor, split_sizes, axis=1)
      return {fn_name[:-3]:t for t, fn_name in zip(x, PMStateWrapper.features_extractors)}

  @staticmethod
  def get_observation_metadata(raw_observation):
    final_obs = []
    for obs in raw_observation:
      o = []
      for extractor_name in PMStateWrapper.features_extractors:
        extractor_fn = PMStateWrapper.extractors_fns[extractor_name]
        o.extend(extractor_fn(obs).shape)
      final_obs.append(o)

    return final_obs


  def __init__(self, env, extractors):
    gym.ObservationWrapper.__init__(self, env)
    PMStateWrapper.features_extractors = extractors
    obs_ = env.reset()
    meta_data = self.get_observation_metadata(obs_)
    PMStateWrapper.features_dims = meta_data
    size = np.sum(np.asarray(meta_data))
    shape = (size,)
    self.observation_space = gym.spaces.Box(
        low=-100, high=100, shape=shape, dtype=np.float32)



  def get_observation_from_extractors(self, raw_observation):
    final_obs = []
    for obs in raw_observation:
      o = []
      for extractor_name in self.features_extractors:
        extractor_fn = PMStateWrapper.extractors_fns[extractor_name]
        o.extend(extractor_fn(obs))
      final_obs.append(o)

    final_obs = final_obs[0] # This will not work with two teams
    return np.array(final_obs, dtype=np.float32)

  def reset(self, **kwargs):
      observation = self.env.reset(**kwargs)
      return self.observation(observation)


  def step(self, action):
      observation, reward, done, info = self.env.step(action)
      info['score'] = observation[0]['score']
      info['game_mode'] = observation[0]['game_mode']
      return self.observation(observation), reward, done, info

  def observation(self, observation):
      return self.get_observation_from_extractors(observation)