import numpy as np

pos = np.array([[1, 2, 3, 4, 5, 6]])
active = np.array([[0, 1, 0]])
dir_ = np.array(range(7, 13))


def team_build(pos, dir_, active_index=None):
    pos = np.reshape(pos, (-1, 2))
    dir_ =  np.reshape(dir_, (-1, 2))
    data = [pos, dir_]
    if active_index:
        one_shot_active = np.zeros((pos.shape[0], 1))
        one_shot_active[active_index] = 1
        data.append(one_shot_active)

    return np.reshape(np.concatenate(data, axis=1), (-1,))

print(team_build(pos, dir_, 1))