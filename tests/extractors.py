import numpy as np
import gfootball.env as football_env
from gfootball.examples.wrappers import team_build_v1

env = football_env.create_environment(env_name="11_vs_11_stochastic",
                                      representation='raw')


obs = env.reset()
obs = obs[0]
res1 = team_build_v1(obs['left_team'], obs['left_team_direction'], 0, obs['ball'], obs['ball_direction'], obs['active'])
res2 = team_build_v1(obs['right_team'], obs['right_team_direction'], 1, obs['ball'], obs['ball_direction'], None)

# [obs['left_team'].flatten(), obs['left_team_direction'].flatten(),
#                                                 obs['right_team'].flatten(), obs['right_team_direction'].flatten(),
#                                                 obs['ball'], obs['ball_direction'],
#                                                 one_hot(obs['ball_owned_team']+1, 3), one_hot(obs['active'], 11),
#                                                 one_hot(obs['game_mode'], 7)]


#'ball', 'ball_owned_team', 'ball_rotation', 'ball_direction', 'ball_owned_player'
#'right_team' (positions), 'right_team_direction', 'right_team_tired_factor', 'right_team_roles', 'right_team_yellow_card',  'right_team_active' (boolean)
#'game_mode', 'steps_left', , 'score',  , 'sticky_actions']

# 'active' - is supposedly active player ( {0, 1, ..., 10} )