import gin

#This is hack due to some compatibility issues
@gin.configurable
def get_PMStateWrapper(env, extractors):
    #Lazy import
    from gfootball.examples.wrappers import PMStateWrapper

    return PMStateWrapper(env, extractors)
